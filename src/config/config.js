const mongoose = require("mongoose");

// Create connection to database

// const connection = new Connection(config);

// Attempt to connect
// connection.on("connect", (err) => {
//   if (err) {
//     console.error(err.message);
//   } else {
//     console.log("Connection established");
//     // listTables();
//   }
// });

// const listTables = () => {
//   const request = new Request(`SELECT * FROM INFORMATION_SCHEMA.TABLES;`, (err, rowCount) => {
//     if (err) {
//       console.error(err.message);
//     } else {
//       console.log(`${rowCount} row(s) returned`);
//     }
//   });
//   console.log("TABLES:");
//   request.on("row", (columns) => {
//     columns.forEach((column) => {
//       if (column.metadata.colName == "TABLE_NAME") console.log("%s\t%s", column.metadata.colName, column.value);
//     });
//   });
//   // connection.execSql(request);
// };

// function queryDatabase() {
//   console.log("Reading rows from the Table...");

//   // Read all rows from table
//   const request = new Request(`SELECT * FROM joel.dashboard_add_to_cart;`, (err, rowCount) => {
//     if (err) {
//       console.error(err.message);
//     } else {
//       console.log(`${rowCount} row(s) returned`);
//     }
//   });

//   let columnData = [];
//   request.on("row", (columns) => {
//     let data = {};
//     columns.forEach((column) => {
//       data[`${column.metadata.colName}`] = column.value;
//       //   console.log("%s\t%s", column.metadata.colName, column.value);
//     });
//     columnData.push(data);
//   });

//   //   request.on("doneInProc", function (rowCount, more, rows) {
//   //     console.log(columnData, rows);
//   //   });

//   request.on("doneProc", function (rowCount, more, returnStatus, rows) {
//     console.log(columnData, rows);
//   });

//   connection.execSql(request);
// }

// exports.query = (sqlQuery, callback) => {
//   const connection = new Connection(config);
//   connection.connect();
//   connection.on("connect", (err) => {
//     if (err) {
//       console.error(err.message);
//     } else {
//       let data = [];
//       const request = new Request(sqlQuery, (err, rowCount) => {
//         if (err) {
//           console.log(err);
//           callback(err, null);
//         } else {
//           connection.close();
//           callback(null, { rows: data, rowCount });
//         }
//       });

//       request.on("row", (columns) => {
//         let colData = {};
//         columns.forEach((column) => {
//           colData[`${column.metadata.colName}`] = column.value;
//         });
//         data.push(colData);
//       });
//       connection.execSql(request);
//     }
//   });

//   // let data = [];
//   // const request = new Request(sqlQuery, (err, rowCount) => {
//   //   if (err) {
//   //     console.log(err);
//   //     callback(err, null);
//   //   } else {
//   //     console.log(`Executed Statement ${Date.now()}`);
//   //     callback(null, { rows: data, rowCount });
//   //     console.log(`${rowCount} row(s) returned`);
//   //   }
//   // });

//   // request.on("row", (columns) => {
//   //   let colData = {};
//   //   columns.forEach((column) => {
//   //     colData[`${column.metadata.colName}`] = column.value;
//   //   });
//   //   data.push(colData);
//   // });

//   //   request.on("doneInProc", function (rowCount, more, rows) {
//   //     // console.log(rows);
//   //     callback(null, { rows: data, rowCount });
//   //   });

//   //   request.on("doneProc", function (rowCount, more, returnStatus, rows) {
//   //     console.log(returnStatus);
//   //     callback(null, { rows: data, rowCount });
//   //   });

//   // connection.execSql(request);
// };

mongoose
  .connect("mongodb://127.0.0.1:27017/PMS")
  .then(() => {
    console.log("DB connected");
  })
  .catch((e) => {
    console.log(e.message);
  });
