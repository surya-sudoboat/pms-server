module.exports = {
  BASE_URL: "/api",
  SCORE_CARD: "/score-card",
  VISITATION: "/visitation",
};
