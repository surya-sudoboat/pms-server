const cryptoStr = require("@supercharge/strings");
const bcrypt = require("bcrypt");
const jbuilder = require("jbuilder");
const { DATE_FORMAT, TABLE_NAMES, TABLE_SCHEMA } = require("../utilities/constants");
const moment = require("moment");
const { ROLES } = require("./constants");
const { BASE_URL } = require("./routes");

const saltRounds = 10;

// Get API Url with base url
const getUrl = (url) => {
  if (url) {
    return `${BASE_URL}${url}`;
  }
  return BASE_URL;
};

// Check if current user is admin
const isAdmin = (roles) => roles && (roles.indexOf(ROLES.superadmin) > -1 || roles.indexOf(ROLES.admin) > -1);

// Generate a unique crypt string for Model ID
const generateCryptId = () => {
  const length = 25;
  return cryptoStr.random(length);
};

// Common handler for API response
const responseFormatter = (res, error, data, meta = null) => {
  if (error) {
    res.status(400);
    res.send({ error });
  } else {
    Promise.resolve(data).then((value) => {
      if (!value) {
        res.status(204);
        res.send({ data: null });
      } else {
        const response = { data: value };
        if (meta) {
          response.meta = {
            page: meta.page,
            limit: meta.limit,
            total_count: meta.total,
            total_pages: meta.pages,
          };
        }
        res.send(response);
      }
    });
  }
};

// Common handler for pagination config
const paginateConfig = (req, otherConfig) => ({
  page: parseInt(req.query.page) || 1,
  limit: parseInt(req.query.limit) || 20,
  ...otherConfig,
});

// Common handler for Deleting records
const deleteRecord = (res, err) => {
  responseFormatter(res, err, { message: "Deletion successful" });
};

// Common handler for Updating records
const updateRecord = (res, err) => {
  responseFormatter(res, err, { message: "Updation successful" });
};

// Common handler for Creating records
const createRecord = (res, err) => {
  responseFormatter(res, err, { message: "Successfully created" });
};

// Encrypt the password
const cryptPassword = (request, res, callback) => {
  if (request.password && request.password === request.password_confirmation) {
    bcrypt.hash(request.password, saltRounds).then((hash) => {
      request.password = hash;
      callback();
    });
  } else {
    responseFormatter(res, { message: "Invalid Password" }, null);
  }
};

// Compare password text with encrypted password
const comparePassword = (password, user, callback) => {
  bcrypt.compare(password, user.password, (err, isPasswordMatch) => {
    callback(err == null && isPasswordMatch);
  });
};

const formRequest = (data, attibutes) => {
  if (!data) return null;

  const output = jbuilder.encode((json) => {
    json.extract(data, ...attibutes);
  });

  return JSON.parse(output);
};

const round_off = (number, decimals = 2) => Math.round(number * 10 ** decimals) / 10 ** decimals;

const getToday = () => moment().format(DATE_FORMAT);

const percentOf = (value, totalValue) => round_off((Number(value) / Number(totalValue)) * 100, 1);

const convertObjectKeysToLower = (data) => {
  const convertedData = {};
  Object.keys(data).forEach((key) => {
    convertedData[key.toLocaleLowerCase()] = data[key];
  });

  return convertedData;
};

const convertArrayOfObjectsKeyToLower = (data) => {
  const convertedData = [];
  data.forEach((row) => {
    convertedData.push(convertObjectKeysToLower(row));
  });
  return convertedData;
};

const getTodayQuery = () => {
  const today = getToday();
  return `SELECT "date", "Week" AS week, "Year_short" AS year_short, "Year" as year, "Month" AS month, fiscal_year FROM defaults.${TABLE_NAMES.dates} WHERE date = '${today}'`;
};

const getFullFiscalYear = (data) => parseInt(data.year.toString().slice(0, 2) + data.fiscal_year);

module.exports = {
  getUrl,
  isAdmin,
  generateCryptId,
  responseFormatter,
  deleteRecord,
  updateRecord,
  cryptPassword,
  comparePassword,
  formRequest,
  paginateConfig,
  createRecord,
  round_off,
  getToday,
  percentOf,
  convertObjectKeysToLower,
  convertArrayOfObjectsKeyToLower,
  getTodayQuery,
  getFullFiscalYear,
};
