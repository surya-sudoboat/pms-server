module.exports = {
  ENV: "development",
  DATE_FORMAT: "YYYY-MM-DD",
  editorCommonUrl: "/api/v1/editor",
  readerCommonUrl: "api/v1/reader",
};
