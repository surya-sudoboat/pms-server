const mongoose = require("mongoose");
// const validator = require("validator");

const templateSchema = mongoose.Schema(
  {
    name: {
      type: String,
    },
    children: {
      type: Array,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("template", templateSchema);
