const express = require("express");
var fs = require("fs");
const template = require("../models/template");
const axios = require("axios");

const { openUsiPopUp } = require("../utilities/helpers");

const router = express.Router();
require("isomorphic-fetch");

// const contentful = require('contentful-management');
// const contentful = require('contentful');
const { response, json } = require("express");




//to Create Script
const makeScript = (_json,index) => {
  

  return `<script >
    const modalHandler${_json.id}${index} = (modalContentType,index) =>{
      console.log(index,modalContentType)
      if(document.getElementById("${index}").className=="modal"){
        document.getElementById("${index}").className = "hideModal";
        if(modalContentType=="cart"){
          let data='<div class="modalContentTitle">ÄPPLARÖ</div><div class="modalContentSubTiltle">Table+8 reclining chairs, outdoor, brown stained</div><div class="modalContetAmount">$ 959.00</div><div class="modalContentButton">See more at www.ikea.com/ca/en/</div><div class="modalContentDescription">Beauty that lasts, from sustainably sourced acacia. This ÄPPLARÖ dining set invites guests to gather for a meal with friends. Just sit back and relax in the comfortable reclining chair.</div>'
          document.getElementById("modal-data-${index}").innerHTML=data;
        }
        else{
          let data='<iframe width="100%" height="100%" src="${"https://www.youtube.com/embed/I7O4IMWcm14"}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>'
          document.getElementById("modal-data-${index}").innerHTML=data;
        }
      }else{        
        document.getElementById("${index}").className = "modal";
      }
    }

    
  </script>`;
};

//To create Style Stringp

const makeStyle = (obj) => {
  // console.log(obj,'sss')
  const styleString = Object.entries(obj)
    .map(([k, v]) => `${k}:${v}`)
    .join(";");
  return "position:absolute;" + styleString;
};

const makeUsiDiv = (jsonData,index,jsonId) => {
  let div = "";
  {
    (jsonData.hotsopts || []).map((x) => {
      div += `<div
          className="hoverdiv"
          id="${x.id}"
          key="${x.id}"
          style="display: flex;z-index: 121;position: absolute;align-items: center;justify-content: center;left: ${
            x.box.left * 100
          }%;top: ${x.box.top * 100}%;bottom: ${100 - x.box.bottom * 100}%;right: ${100 - x.box.right * 100}%;" 
        onclick='modalHandler${jsonId}${index}("${x.clickEvent}",${index})'
        onmouseenter='document.getElementById(event.target.id).style.backgroundColor="rgb(255 255 255 / 33%)"' onmouseleave='document.getElementById(event.target.id).style.backgroundColor="transparent"'
        >
          <div
            style=
              'position: absolute;
              display: flex;
              height: 40px;
              background-color: #0000004d;
              border-radius: 50px;
              width: 40px;
              top: ${x.hotSpot.top * 100}%;
              left: ${x.hotSpot.left * 100}%;             
              transform: translate(-50%, -50%);
              align-items: center;
              justify-content: center;'
          >
          <div
          style='
          height: 20px;
          border-radius: 50px;
          background-color: white;
          width: 20px;'
        ></div>
          </div>
        </div>`;
    });
  }
  return div;
};

//To create div element
const makeDiv = (jsonData,index,jsonId) => {
  let style = makeStyle(jsonData.styles);
  // console.log(jsonData, "ass");
  let div;
  if (jsonData.type == "image") {
    if (!jsonData.event) {
      div = `<${jsonData.tag} style="top:${jsonData.styles.top}px;left:${jsonData.styles.left}px;width:${jsonData.styles.width}px;height:${jsonData.styles.height}px !important;position:absolute;
      transform:translate(${jsonData.styles.left}px,${jsonData.styles.top}px)" 
      src='${jsonData.value}  '/>`;
    } else {
      div = `<${jsonData.tag} style="top:${jsonData.styles.top}px;left:${jsonData.styles.left}px;width:${jsonData.styles.width}px;height:${jsonData.styles.height}px !important;position:absolute;
      transform:translate(${jsonData.styles.left}px,${jsonData.styles.top}px)"
      src='${jsonData.value}'/>`;
    }
  } else if (jsonData.type == "usi_image") {
    div = `<div style="display:flex;background-color:white;top:${jsonData.styles.top}px;left:${
      jsonData.styles.left
    }px;width:${jsonData.styles.width}px;height:${jsonData.styles.height}px !important;position:absolute;
    transform:translate(${jsonData.styles.left}px,${jsonData.styles.top}px)">
    <img src="${jsonData.value}" alt={} style="height:100%;width:100%"/>
    ${makeUsiDiv(jsonData,index,jsonId)}
   </div>`;
  } else if(jsonData.type=="rich_text"){
    div = `<div style="top:${jsonData.styles.top}px;left:${jsonData.styles.left}px;width:${jsonData.styles.width}px;height:${jsonData.styles.height}px;position:absolute;
    transform:translate(${jsonData.styles.left}px,${jsonData.styles.top}px)" >${jsonData.htmlString}</div>`
  }
  else {
    div = `<${jsonData.tag} style="top:${jsonData.styles.top}px;left:${jsonData.styles.left}px;width:${jsonData.styles.width}px;height:${jsonData.styles.height}px;position:absolute;
    transform:translate(${jsonData.styles.left}px,${jsonData.styles.top}px)" >${jsonData.value}</${jsonData.tag}>`;
  }
  // console.log(div, "div");
  return div;
};


/* make html to render in the front end */
/* here two function has been used to make html */
/* createHtmlForPage function is used to get page json from the contentful */
/* GenerateHtml is for baking html code with the page josn */

const createHtmlForPages = async(data) =>{
  const htmlData = data.map(async(_x)=>{
    query = `query pagesEntryQuery {
      pages(id: "${_x.sys.id}") {
        pageJson
      }
    }`;
    const response = await fetch("https://graphql.contentful.com/content/v1/spaces/qfw7d55fyp9q?access_token=PKEdHms5HQdHolLRntCXedImPMIIpTYStWZIR-y-HRs&order=publishDate",
          {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
              "Access-Control-Allow-Origin": "*",
            },
            body: JSON.stringify({ query }),
          }
      );
      return response.json()
  })
  const results = await Promise.all(htmlData);
  // console.log(results,"result")
  const pageJsons = results.map((_x)=>_x.data.pages.pageJson)
  const htmlFilse = generateHtml(pageJsons)
  return htmlFilse;
}

const generateHtml = (Json) =>{
  const htmlFiles = []
  Json.forEach((_json,index)=>{
    let htmlDivData = "";
    if(_json && _json.children){
      jsonArray = _json.children ;
      (jsonArray||[]).forEach((element) => {
        htmlDivData += makeDiv(element,index,_json.id);
      });
    }
    let modalConatiner = `<div id="${index}" class="modal">
  
        <!-- Modal content -->
        <div class="modal-content">
          <div class="closeContainer">
             <div onclick='document.getElementById("${index}").className = "modal"' class="close">&times;</div></div>
             <div id="modal-data-${index}" class="modal-data modal-data-${index}"></div>
          </div>
        </div>`;

        let modalStyle = `
      <style>
        p{
          margin:unset !important;
        }
        .modal{
          display:none;
          text-align:center
        }
        .hideModal{
          width:400px;
          top:25%;
          height:500px;
          left:25%;
          display:block;
        }
        #index{
          z-index:200;
          position:absolute;
          background-color:grey;
        }
        .modal-content{
          display: flex;
          width:100%;
          height:100%;
          flex-direction: column;
          box-shadow: rgb(0 0 0 / 5%) 0px 6px 24px 0px, rgb(0 0 0 / 30%) 0px 0px 0px 1px;
          padding: 20px;
          background-color:white;
        }
        .modalContentTitle{
          font-size: 22px;            
          font-weight: bold;
        }
        .modalContentSubTiltle{
          font-size: 22px;
        }
        .modalContetAmount{
          font-size: 38px;
          font-weight: bold;
        }
        .modalContentButton{
          background-color: #0058a3;
          width: fit-content;
          color: white;
          border-radius: 4px;
          padding: 17px;
          cursor: pointer;
        }
        .modal-data{
          width:100%;
          height:100%;
          display: flex;
          flex-direction: column;
          gap: 20px;
        }
        .closeContainer{
          display: flex;
          justify-content: flex-end;
          font-size: 26px;
          cursor:pointer;
        }
        body{
         font-family:-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, 'Helvetica Neue', Arial, 'Noto Sans', sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol', 'Noto Color Emoji' !important;
        }
      
      </style>
        `;
          let script = makeScript(_json,index);
          let htmlData;
    
        //old response html

          htmlData = `<html style="height:100%;width:100%">
                            <head>${modalStyle}</head>
                               <body style="height:100%;width:100%;margin:auto" >
                                 <div style="position:relative"><div style="position:relative;height:297mm;width:211mm">
                                    ${htmlDivData}
                                    ${modalConatiner}
                                  </div>
                                  ${script}
                                </body>
                        </html>`;
        
          htmlFiles.push(htmlData)
  })
  return htmlFiles
}


router.get("/html/:id", async (req, res) => {
  const query = `query publicationEntryQuery {
    publication(id: "${req.params.id}") {
      linkedFrom{
        pagesCollection{
          items{
            sys{
              id
            },
          }
        }
      }
    }
  }`;
  try {
    fetch(
      "https://graphql.contentful.com/content/v1/spaces/qfw7d55fyp9q?access_token=PKEdHms5HQdHolLRntCXedImPMIIpTYStWZIR-y-HRs",
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        "Access-Control-Allow-Origin": "*",
        body: JSON.stringify({ query }),
      }
    )
      .then((response) => response.json())
      .then(async(json) => {
        const responses = await createHtmlForPages(json.data.publication.linkedFrom.pagesCollection.items);
        res.send(responses).status(200);
      });
  } catch (err) {
    res.send(err).status(400);
  }
});


const makeProductJson = (data) =>{
  let json = [];
  data.forEach((item) => {
    json.push({
      PRODUCT_NAME: item.prod_Name,
      PRODUCT_TYPE:item.prodtype_Name,
      CURRENCY_CODE: item.currency_Code,
      SHORT_MATERIAL_TEXT: item.short_Material_Text,
      VALID_DESIGN_TEXT:item.valid_Design_Text,
      ITEM_NAME: item.item_Name,
      ITEM_NO: item.item_No,
      SALES_PRICE: item.rec_Sales_Price,
    })
  })
  return json
}

const createProductJson = async(data) =>{
  const productdata = data.map(async(_x)=>{
    
    const response = await fetch(`https://icomapi.azure-api.net/mix-prod/api/v2/pia/items/${_x}`,
        {
          method: "GET",
          headers: {
            'Cache-Control': 'no-cache',
            'Ocp-Apim-Subscription-Key': '54e6d24b938545349bfb74fd28315c8d',
          },
        }
      );
      return response.json()
  })
  const results = await Promise.all(productdata);
  const json = makeProductJson(results)
  return json;
}


const filterData = (data) =>{
  let filterText = "ART"
  return data.filter((item,index)=>item.includes(filterText) && index < 40)
}


// router.get('/product',async (req, res) =>{
//   console.log("api calling")
//   try{
//     fetch(
//       "https://icomapi.azure-api.net/mix-prod/api/v2/pia/items",
//       {
//         method: "GET",
//         headers: {
//           'Cache-Control': 'no-cache',
//           'Ocp-Apim-Subscription-Key': '54e6d24b938545349bfb74fd28315c8d',
//         },
//       }
//     )
//       .then((response) => response.json())
//       .then(async(json) => {
//         // console.log(json,"thi is json")
//         let itemNumber = filterData(json)
//         console.log(itemNumber)
//         const responses = await createProductJson(itemNumber);
//         res.send(itemNumber).status(200);
//       });
//   } catch (err) {
//     console.log(err);
//   }
// })

router.get('/product',async (req, res) =>{
  let data = [
    "ART-00073100",
    "ART-00079300",
    "ART-80428300",
    "ART-90454100",
    "ART-30451500",
    "ART-00052800",
    "ART-00061300",
    "ART-00065100",
    "ART-00069400",
    "ART-00088700",
    "ART-00101400",
    "ART-00093400",
    "ART-00094800",
    "ART-00109000",
    "ART-00112700",
    "ART-00114600",
    "ART-00116500",
    "ART-10461500",
    "ART-00132500",
    "ART-00128800",
    "ART-00129300",
    "ART-00140500",
    "ART-00153700",
    "ART-00155600",
    "ART-00157500",
    "ART-00163600",
    "ART-00165500",
    "ART-00166000",
    "ART-00167900",
    "ART-00169800",
    "ART-00170200",
    "ART-00159900",
    "ART-00160300",
    "ART-00182000",
    "ART-00175900",
    "ART-00176400",
    "ART-00186300"
]
  try{
    const productdata = data.map(async(_x)=>{
      const response = await fetch(`https://icomapi.azure-api.net/mix-prod/api/v2/pia/items/${_x}`,
          {
            method: "GET",
            headers: {
              'Cache-Control': 'no-cache',
              'Ocp-Apim-Subscription-Key': '54e6d24b938545349bfb74fd28315c8d',
            },
          }
        );
        return response.json()
    })
    const results = await Promise.all(productdata);
    const json = makeProductJson(results)
    res.send(json).status(200)
  } catch (err) {
    console.log(err);
  }
})




module.exports = router;
