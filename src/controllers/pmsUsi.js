const express = require("express");
const axios = require("axios");
const router = express.Router();

router.get("/:id", async (req, res) => {
  const header = {
    "X-Auth-Token": "130af51fd2ca4e638f4e30182d839c0d",
  };

  try {
   
    let data = await axios.get(`https://idam.inter.ikea.com/api/entities/identifier/M.Asset.${req.params.id}`, {
      headers: header,
    });
    let assetPublicLink = await axios.get(`${data.data.relations.AssetToPublicLink.href}`,{
        headers: header,
      })
    let publicLink = await axios.get(`${assetPublicLink.data.children[1].href}`,{
      headers: header,
    })
    res.send({href:publicLink.data.public_link});
  } catch (err) {
    res.status(400).json(err.message);
  }
});

module.exports = router;
