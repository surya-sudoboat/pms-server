// .env
// require("dotenv").config();

const { getUrl } = require("./src/utilities/helpers");

const mongoose = require("mongoose");
const cors = require("cors");

// Urls
const { BASE_URL, SCORE_CARD, VISITATION } = require("./src/utilities/routes");

const { readerCommonUrl } = require("./src/utilities/constants");

// controllers

// Dashboard

const PmsReaderPath = require("./src/controllers/pmsReader");
const PmsUsiPath = require("./src/controllers/pmsUsi");

// Visitaiton
const express = require("express");
const app = express();

// CORS handling
const whitelist = [`http://localhost:8003`];
const corsOptions = {
  origin: (origin, callback) => {
    if (whitelist.indexOf(origin) !== -1 || !origin || origin == "null") {
      callback(null, true);
    } else {
      callback(new Error("Not allowed by CORS"));
    }
  },
  credentials: true,
};

app.use(cors());

// app.use(cors());\

app.use(function (req, res, next) {
  // res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
  // res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// Parse all request data to json
const bodyParser = require("body-parser");
app.use(bodyParser.json());

//Routers
app.use("/api/v1/reader", PmsReaderPath);
app.use("/api/v1/usi", PmsUsiPath);

console.log("App is now ready on localhost:9000");
app.listen(9000);
mongoose
  .connect("mongodb://127.0.0.1:27017/PMS")
  .then(() => {
    console.log("DB connected");
  })
  .catch((e) => {
    // console.log(e.message);
  });
